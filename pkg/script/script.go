package script

import (
	"bufio"
	"bytes"
	"log"
)

func Load(script []byte) {
	scanner := bufio.NewScanner(bytes.NewReader(script))

	for scanner.Scan() {
		log.Println(scanner.Text())
	}
}
