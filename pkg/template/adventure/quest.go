package adventure

type Quest struct {
	Discovered  bool
	Title       string
	Description string
}

func (q *Quest) Complete() bool {
	return false
}
