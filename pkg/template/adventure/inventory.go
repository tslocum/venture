package adventure

type Inventory struct {
	Size  int
	Items []*Item
}

func (i *Inventory) Add(item *Item) bool {
	if i.Size <= len(i.Items) {
		return false
	}

	i.Items = append(i.Items, item)
	return true
}

func (i *Inventory) Contains(item *Item) bool {
	for _, searchItem := range i.Items {
		if searchItem == item {
			return true
		}
	}
	return false
}
