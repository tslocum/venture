package adventure

import (
	"github.com/hajimehoshi/ebiten"
	"gitlab.com/tslocum/venture/pkg/engine"
	"gitlab.com/tslocum/venture/pkg/world"
)

type Game struct {
	*engine.Game

	Root *world.Scene // TODO
}

func NewAdventureGame() *Game {
	baseGame := engine.NewGame()

	g := Game{Game: baseGame, Root: world.NewScene(nil)}

	baseGame.UpdateFunc = g.UpdateGame
	baseGame.SetRoot(g.Root)

	return &g
}

func (g *Game) UpdateGame(screen *ebiten.Image) error {
	return nil
}

func (g *Game) AddObject(object Object) {
	g.Root.AddNode(object)
}
