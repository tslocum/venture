/*
Package adventure provides a graphic adventure (point and click) game template.

Verbs

Verbs describe the ways players and objects interact with each other and the
world.  When no specific action is chosen, VerbDefault is used.
*/
package adventure
