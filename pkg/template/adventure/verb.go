package adventure

// Verb represents an action. Use a negative value when creating custom verbs.
type Verb int

const (
	VerbNone     Verb = 0
	VerbDefault  Verb = 1
	VerbLook     Verb = 2
	VerbInteract Verb = 3
)
