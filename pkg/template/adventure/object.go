package adventure

import "gitlab.com/tslocum/venture/pkg/world"

type Object interface {
	world.Node

	ObjectName() string
	ObjectType() int

	// WorldSprite is the sprite displayed when this object is not held in the player's inventory.
	WorldSprite() *world.Sprite
}
