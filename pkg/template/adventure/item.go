package adventure

import "gitlab.com/tslocum/venture/pkg/world"

type Item interface {
	Object

	// ItemSprite is the sprite displayed when the item is held by the player.
	ItemSprite() *world.Sprite
}
