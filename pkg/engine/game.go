package engine

import (
	"errors"
	"fmt"
	"sync"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"github.com/hajimehoshi/ebiten/inpututil"
	"gitlab.com/tslocum/venture/pkg/world"
)

// ErrExit is the error returned when exiting normally.
var ErrExit = errors.New("exit")

// Game is a venture game.
type Game struct {
	UpdateFunc func(screen *ebiten.Image) error

	rootNode world.Node
	sync.Mutex
}

// NewGame returns a new venture game.
func NewGame() *Game {
	doNothing := func(screen *ebiten.Image) error {
		return nil
	}

	g := &Game{UpdateFunc: doNothing}
	return g
}

// SetRoot sets the root node of the game.
func (g *Game) SetRoot(node world.Node) {
	g.Lock()
	defer g.Unlock()

	g.rootNode = node
}

// Update updates the game.
func (g *Game) Update(screen *ebiten.Image) error {
	g.Lock()
	defer g.Unlock()

	if g.rootNode != nil {
		g.fireMouseEvents()
	}

	err := g.UpdateFunc(screen)
	if err != nil {
		return err
	}

	if g.rootNode == nil {
		return nil
	}

	err = g.updateNode(screen, g.rootNode)
	if err != nil {
		return err
	}

	return nil
}

func (g *Game) fireMouseEvents() {
	x, y := ebiten.CursorPosition()
	hitNodes := g.nodesAt(float64(x), float64(y), g.rootNode)
	if hitNodes == nil {
		return
	}

	leftDown := inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft)
	middleDown := inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonMiddle)
	rightDown := inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonRight)

	leftUp := inpututil.IsMouseButtonJustReleased(ebiten.MouseButtonLeft)
	middleUp := inpututil.IsMouseButtonJustReleased(ebiten.MouseButtonMiddle)
	rightUp := inpututil.IsMouseButtonJustReleased(ebiten.MouseButtonRight)

	for i := len(hitNodes) - 1; i >= 0; i-- {
		if leftDown || middleDown || rightDown {
			newNode, ok := hitNodes[i].(world.HandlesMouseDown)
			if ok && ((leftDown && !newNode.MouseDown(float64(x), float64(y), ebiten.MouseButtonLeft)) ||
				(middleDown && !newNode.MouseDown(float64(x), float64(y), ebiten.MouseButtonMiddle)) ||
				(rightDown && !newNode.MouseDown(float64(x), float64(y), ebiten.MouseButtonRight))) {
				return
			}
		}

		if leftUp || middleUp || rightUp {
			newNode, ok := hitNodes[i].(world.HandlesMouseUp)
			if ok && ((leftUp && !newNode.MouseUp(float64(x), float64(y), ebiten.MouseButtonLeft)) ||
				(middleUp && !newNode.MouseUp(float64(x), float64(y), ebiten.MouseButtonMiddle)) ||
				(rightUp && !newNode.MouseUp(float64(x), float64(y), ebiten.MouseButtonRight))) {
				return
			}
		}

		newNode, ok := hitNodes[i].(world.HandlesHover)
		if ok && !newNode.Hover(float64(x), float64(y)) {
			return
		}
	}
}

func (g *Game) updateNode(screen *ebiten.Image, node world.Node) error {
	childNodes := node.Nodes()
	for i := 0; i < len(childNodes); i++ {
		err := g.updateNode(screen, childNodes[i])
		if err != nil {
			return err
		}
	}

	err := node.Update(screen)
	if err != nil {
		return err
	}

	return nil
}

// Draw draws the game.
func (g *Game) Draw(screen *ebiten.Image) {
	g.Lock()
	defer g.Unlock()

	var nodes int
	if g.rootNode != nil {
		nodes = g.drawNode(screen, g.rootNode)
	}

	ebitenutil.DebugPrint(screen, fmt.Sprintf("FPS    %0.0f\nTPS    %0.0f\nNodes  %d", ebiten.CurrentFPS(), ebiten.CurrentTPS(), nodes))
}

func (g *Game) drawNode(screen *ebiten.Image, node world.Node) int {
	nodes := 1
	node.Draw(screen)

	childNodes := node.Nodes()
	for i := 0; i < len(childNodes); i++ {
		nodes += g.drawNode(screen, childNodes[i])
	}

	return nodes
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	s := ebiten.DeviceScaleFactor()
	return int(float64(outsideWidth) * s), int(float64(outsideHeight) * s)
}

func (g *Game) Run() error {
	err := ebiten.RunGame(g)
	if err != nil && err != ErrExit {
		return err
	}
	return nil
}

func (g *Game) NodeAt(x float64, y float64) []world.Node {
	g.Lock()
	defer g.Unlock()

	return g.nodesAt(x, y, g.rootNode)
}

func (g *Game) nodesAt(x float64, y float64, node world.Node) []world.Node {
	var n []world.Node

	if node.HitTest(x, y) {
		n = append(n, node)
	}

	for _, child := range node.Nodes() {
		n = append(n, g.nodesAt(x, y, child)...)
	}

	return n
}
