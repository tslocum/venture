package world

import (
	"github.com/hajimehoshi/ebiten"
)

// Node is an object.
type Node interface {
	AddNode(Node)
	HitTest(x float64, y float64) bool
	Nodes() []Node
	Update(screen *ebiten.Image) error
	Draw(screen *ebiten.Image)
}

type HandlesHover interface {
	Hover(x float64, y float64) bool
}

type HandlesMouseDown interface {
	MouseDown(x float64, y float64, button ebiten.MouseButton) bool
}

type HandlesMouseUp interface {
	MouseUp(x float64, y float64, button ebiten.MouseButton) bool
}
