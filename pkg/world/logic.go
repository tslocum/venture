package world

import (
	"sync"

	"github.com/hajimehoshi/ebiten"
)

// Logic is an invisible node.
type Logic struct {
	UpdateFunc func(screen *ebiten.Image) error

	nodes []Node
	sync.Mutex
}

// NewLogic returns a new Logic node.
func NewLogic(updateFunc func(screen *ebiten.Image) error) *Logic {
	if updateFunc == nil {
		updateFunc = func(screen *ebiten.Image) error {
			return nil
		}
	}
	return &Logic{UpdateFunc: updateFunc}
}

// AddNode adds a node.
func (l *Logic) AddNode(node Node) {
	l.Lock()
	defer l.Unlock()

	l.nodes = append(l.nodes, node)
}

func (l *Logic) Hit(x float64, y float64) bool {
	return true
}

func (l *Logic) HitTest(x float64, y float64) bool {
	return false
}

// Nodes returns any contained nodes.
func (l *Logic) Nodes() []Node {
	l.Lock()
	defer l.Unlock()

	return l.nodes
}

// Update calls UpdateFunc.
func (l *Logic) Update(screen *ebiten.Image) error {
	return l.UpdateFunc(screen)
}

// Draw does nothing.
func (l *Logic) Draw(screen *ebiten.Image) {
	// Do nothing
}
