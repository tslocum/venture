package world

import (
	"image/color"
	"log"

	"github.com/hajimehoshi/ebiten"
)

// Sprite is a graphical node.
type Sprite struct {
	*Logic
	Image  *ebiten.Image
	Images []*ebiten.Image

	Visible bool

	w, h                 float64
	x, y                 float64
	scaleX, scaleY       float64
	originalW, originalH float64
	colorm               *ebiten.ColorM
	opts                 *ebiten.DrawImageOptions
	err                  error
}

// NewSprite returns a new Sprite node.
func NewSprite(image *ebiten.Image, updateFunc func(screen *ebiten.Image) error) *Sprite {
	w, h := image.Size()

	s := &Sprite{Logic: NewLogic(updateFunc), Image: image, Images: []*ebiten.Image{image}, Visible: true, originalW: float64(w), originalH: float64(h), scaleX: 1, scaleY: 1, w: float64(w), h: float64(h), colorm: &ebiten.ColorM{}, opts: &ebiten.DrawImageOptions{}}
	return s
}

// SetPosition sets the position of the node.
func (s *Sprite) SetScale(x float64, y float64) {
	s.Lock()
	defer s.Unlock()

	s.scaleX = x
	s.scaleY = y

	s.w = s.originalW * s.scaleX
	s.h = s.originalH * s.scaleY

	s.updateDrawOptions()
}

// SetPosition sets the position of the node.
func (s *Sprite) GetScale() (float64, float64) {
	s.Lock()
	defer s.Unlock()

	return s.scaleX, s.scaleY
}

// SetPosition sets the position of the node.
func (s *Sprite) SetImage(image *ebiten.Image) {
	s.Lock()
	defer s.Unlock()

	s.Image = image
}

// SetPosition sets the position of the node.
func (s *Sprite) SetPosition(x float64, y float64) {
	s.Lock()
	defer s.Unlock()

	s.x = x
	s.y = y

	s.updateDrawOptions()
}

// AddPosition adds to the position of the node.
func (s *Sprite) AddPosition(x float64, y float64) {
	s.Lock()
	defer s.Unlock()

	s.x += x
	s.y += y

	if s.opts != nil {
		s.opts.GeoM.Translate(x, y)
		return
	}

	s.updateDrawOptions()
}

// GetPosition returns the position of the node.
func (s *Sprite) GetPosition() (x float64, y float64) {
	s.Lock()
	defer s.Unlock()

	return s.x, s.y
}

// HitTest returns whether the the specified position is within the sprite bounds.
func (s *Sprite) HitTest(x float64, y float64) bool {
	s.Lock()
	defer s.Unlock()

	return x >= s.x && y >= s.y && x <= s.x+s.w && y <= s.y+s.h
}

// HitExact returns whether the the specified position is within a
// non-transparent area of the sprite.
func (s *Sprite) HitExact(x float64, y float64) bool {
	s.Lock()
	defer s.Unlock()

	return s.Image.At(int(x-s.x), int(y-s.y)).(color.RGBA).A > 0
}

func (s *Sprite) GetSize() (float64, float64) {
	s.Lock()
	defer s.Unlock()

	return s.w, s.h
}

func (s *Sprite) SetColorM(colorm *ebiten.ColorM) {
	s.Lock()
	defer s.Unlock()

	s.colorm = colorm
	s.updateDrawOptions()
}

func (s *Sprite) GetColorM() *ebiten.ColorM {
	s.Lock()
	defer s.Unlock()

	return s.colorm
}

func (s *Sprite) updateDrawOptions() {
	s.opts = &ebiten.DrawImageOptions{}
	s.opts.GeoM.Scale(s.scaleX, s.scaleY)
	s.opts.GeoM.Translate(s.x, s.y)
	s.opts.ColorM = *s.colorm
}

// Draw draws the node.
func (s *Sprite) Draw(screen *ebiten.Image) {
	s.Lock()
	defer s.Unlock()

	if !s.Visible {
		return
	}

	s.err = screen.DrawImage(s.Image, s.opts)
	if s.err != nil {
		log.Fatal(s.err)
	}
}
