package world

import "github.com/hajimehoshi/ebiten"

// Scene is a node which organizes nodes.
type Scene struct {
	*Logic
}

// NewScene returns a new Scene.
func NewScene(updateFunc func(screen *ebiten.Image) error) *Scene {
	return &Scene{NewLogic(updateFunc)}
}

// SetNode sets the nodes of a scene.
func (n *Scene) SetNode(node Node) {
	n.nodes = []Node{node}
}

// SetNodes sets the nodes of a scene.
func (n *Scene) SetNodes(nodes []Node) {
	n.nodes = nodes
}

// RemoveNode removes the provided node from a scene.
func (n *Scene) RemoveNode(node Node) {
	for i, subNode := range n.nodes {
		if subNode == node {
			n.nodes = append(n.nodes[:i], n.nodes[i+1:]...)
			return
		}
	}
}

// Clear removes all nodes from a scene.
func (n *Scene) Clear() {
	n.nodes = nil
}
