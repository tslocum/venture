/*
Package venture provides a game engine powered by ebiten.

Overview

The building blocks of venture games are nodes. Everything displayed on the
screen is a node. For instance, a node could be a single line of text on the
screen, a bouncing ball, or a scene which contains a line of text and a
bouncing ball.

Nodes

Nodes may be any type which implement the following:

    type Node interface {
        AddNode(Node)
        Nodes() []Node
        Update(screen *ebiten.Image) error
        Draw(screen *ebiten.Image)
    }

Three types of nodes are provided by venture:

- Logic, an invisible node

- Sprite, a graphical node

- Scene, an organizational node

Event Loop

Once a venture game has started, with each tick the game's UpdateFunc is
called, followed by calling the Update method of the root node and any
contained nodes.

While a reference to the screen is provided to Update, any modifications must
be made in Draw.
*/
package venture
