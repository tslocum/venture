module gitlab.com/tslocum/venture

go 1.14

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hajimehoshi/ebiten v1.12.0-alpha.1.0.20200524173645-1cfc1964bf3d
	github.com/hajimehoshi/go-mp3 v0.2.2 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
)
