# venture
[![GoDoc](https://gitlab.com/tslocum/godoc-static/-/raw/master/badge.svg)](https://docs.rocketnine.space/gitlab.com/tslocum/venture)
[![CI status](https://gitlab.com/tslocum/venture/badges/master/pipeline.svg)](https://gitlab.com/tslocum/venture/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Game engine powered by [ebiten](https://github.com/hajimehoshi/ebiten)

## Features

- Scenes and their objects are comprised of nested [nodes](https://docs.rocketnine.space/gitlab.com/tslocum/venture/#hdr-Nodes)
- Templates are available to further simplify game creation
  - [adventure](https://docs.rocketnine.space/gitlab.com/tslocum/venture/pkg/template/adventure/) - Graphic adventure (point-and-click) 

## Games

- [Steamed Hams but its a graphic adventure video game](https://gitlab.com/tslocum/steamed-hams-but-its-a-graphic-adventure-video-game)

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/venture/issues).

## Libraries

The following libraries are used to build venture:

* [hajimehoshi/ebiten](https://github.com/hajimehoshi/ebiten) - Game engine
