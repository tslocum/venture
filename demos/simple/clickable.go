package main

import "github.com/hajimehoshi/ebiten"

type clickableLogo struct {
	*logo
}

func newClickableLogo() *clickableLogo {
	l := clickableLogo{newLogo()}
	return &l
}

func (l *clickableLogo) MouseDown(x float64, y float64, button ebiten.MouseButton) bool {
	l.up = !l.up
	return true
}

func (l *clickableLogo) MouseUp(x float64, y float64, button ebiten.MouseButton) bool {
	l.left = !l.left
	return true
}
