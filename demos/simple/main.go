package main

import (
	"log"
	"math/rand"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"gitlab.com/tslocum/venture/pkg/engine"
	"gitlab.com/tslocum/venture/pkg/world"
)

func main() {
	// Initialize

	ebiten.SetWindowTitle("venture demo")

	// TODO add controls
	ebiten.SetVsyncEnabled(false)
	//ebiten.SetMaxTPS(120)
	ebiten.SetMaxTPS(ebiten.UncappedTPS)
	//ebiten.SetMaxTPS(60)

	ebiten.SetFullscreen(true)

	initializeLogo()

	game := engine.NewGame()

	mainScene := world.NewScene(nil)

	// Create scene A

	sceneA := world.NewScene(nil)
	mainScene.SetNode(sceneA)

	sw, sh := ebiten.ScreenSizeInFullscreen()
	addSprites := func() {
		for i := 0; i < 10; i++ {
			logo := newLogo()

			logo.SetPosition(float64(rand.Intn(sw)), float64(rand.Intn(sh)))

			if rand.Intn(2) == 1 {
				logo.left = true
			}

			if rand.Intn(2) == 1 {
				logo.up = true
			}

			sceneA.AddNode(logo)
		}
	}
	addSprites()

	sceneA.AddNode(world.NewLogic(func(screen *ebiten.Image) error {
		if inpututil.IsKeyJustPressed(ebiten.KeyKPAdd) {
			addSprites()
		}
		return nil
	}))

	// Create scene B

	sceneB := world.NewScene(nil)
	sceneB.AddNode(newClickableLogo())

	// Create update loop

	var currentScene int
	game.UpdateFunc = func(screen *ebiten.Image) error {
		if inpututil.IsKeyJustPressed(ebiten.KeyTab) {
			if currentScene == 0 {
				mainScene.SetNode(sceneB)
				currentScene = 1
			} else {
				mainScene.SetNode(sceneA)
				currentScene = 0
			}
		} else if inpututil.IsKeyJustPressed(ebiten.KeyEscape) || (inpututil.KeyPressDuration(ebiten.KeyControl) > 0 && inpututil.IsKeyJustPressed(ebiten.KeyC)) {
			return engine.ErrExit
		}
		return nil
	}

	// Run

	game.SetRoot(mainScene)
	if err := game.Run(); err != nil {
		log.Fatal(err)
	}
}
