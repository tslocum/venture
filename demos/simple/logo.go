package main

import (
	"image/color"
	"log"

	"github.com/golang/freetype/truetype"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/text"
	"gitlab.com/tslocum/venture/pkg/world"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/goregular"
)

var (
	imgLogo          *ebiten.Image
	screenW, screenH float64
)

var goFontFace font.Face

func initializeLogo() {
	goFont, err := truetype.Parse(goregular.TTF)
	if err != nil {
		log.Fatal(err)
	}

	// TODO configurable?
	goFontFace = truetype.NewFace(goFont, &truetype.Options{
		Size:    128,
		DPI:     128,
		Hinting: font.HintingNone,
	})

	textSize := text.MeasureString("venture", goFontFace)

	imgLogo, err = ebiten.NewImage(textSize.X+10, (textSize.Y/2)+40, ebiten.FilterDefault)
	if err != nil {
		log.Fatal(err)
	}

	err = imgLogo.Fill(color.NRGBA{0, 172, 215, 255})
	if err != nil {
		log.Fatal(err)

	}

	text.Draw(imgLogo, "venture", goFontFace, 10, (textSize.Y/2)+22, color.Black)

	sw, sh := ebiten.ScreenSizeInFullscreen()
	screenW, screenH = float64(sw), float64(sh)
}

type logo struct {
	*world.Sprite

	w, h float64

	left bool
	up   bool
}

func newLogo() *logo {
	s := world.NewSprite(imgLogo, nil)
	w, h := imgLogo.Size()
	return &logo{Sprite: s, w: float64(w), h: float64(h)}
}

func (l *logo) Update(screen *ebiten.Image) error {
	posX, posY := l.GetPosition()
	w, h := l.GetSize()

	if posX <= 0 {
		l.left = false
	} else if posX+w >= screenW {
		l.left = true
	}

	if posY <= 0 {
		l.up = false
	} else if posY+h >= screenH {
		l.up = true
	}

	if l.left {
		posX--
	} else {
		posX++
	}

	if l.up {
		posY--
	} else {
		posY++
	}

	l.SetPosition(posX, posY)
	return nil
}
