package main

import (
	"time"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/tslocum/venture/pkg/world"
)

const (
	handlePaddingX    = 20
	handleTopLimit    = 55
	handleBottomLimit = 67
	handleCatchBuffer = 15
)

type ToasterHandle struct {
	*world.Sprite

	toaster *Toaster

	dragging bool
}

func NewToasterHandle(t *Toaster) *ToasterHandle {
	h := &ToasterHandle{Sprite: world.NewSprite(imgToasterHandle, nil), toaster: t}
	return h
}

func (h *ToasterHandle) MouseDown(x float64, y float64, button ebiten.MouseButton) bool {
	if h.toaster.started.IsZero() {
		h.dragging = true
	}

	return false
}

func (h *ToasterHandle) Update(screen *ebiten.Image) error {
	toasterX, toasterY := h.toaster.GetPosition()
	_, toasterH := h.toaster.GetSize()
	_, cursorY := ebiten.CursorPosition()
	_, handleY := h.GetPosition()

	if h.dragging && !ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		h.dragging = false

		if h.toaster.powered && handleY >= (toasterY+toasterH)-handleBottomLimit-handleCatchBuffer {
			h.toaster.started = time.Now()
		}
	}

	var newY float64
	if h.dragging {
		newY = float64(cursorY)
	} else if !h.toaster.started.IsZero() {
		newY = (toasterY + toasterH) - handleBottomLimit
	} else if h.toaster.started.IsZero() && handleY > (toasterY+handleTopLimit) {
		newY = handleY - 17
	} else {
		newY = 0
	}

	if newY < toasterY+handleTopLimit {
		newY = toasterY + handleTopLimit
	} else if newY > (toasterY+toasterH)-handleBottomLimit {
		newY = (toasterY + toasterH) - handleBottomLimit
	}

	breadVisible := newY < toasterY+handleTopLimit+handleCatchBuffer
	for _, bread := range h.toaster.bread {
		bread.Visible = breadVisible
	}

	h.SetPosition(toasterX+handlePaddingX, float64(newY))

	return nil
}

type Toaster struct {
	*world.Sprite

	handle *ToasterHandle

	powered bool
	started time.Time
	bread   []*Bread
}

func NewToaster() *Toaster {
	t := &Toaster{}
	t.handle = NewToasterHandle(t)
	t.handle.SetScale(0.5, 0.5)

	t.Sprite = world.NewSprite(imgToaster, t.Update)

	t.AddNode(t.handle)

	return t
}

func (t *Toaster) WorldSprite() *world.Sprite {
	return t.Sprite
}

func (t *Toaster) ObjectName() string {
	return "Toaster"
}

func (t *Toaster) ObjectType() int {
	return 1 // TODO
}

func (t *Toaster) MouseDown(x float64, y float64, button ebiten.MouseButton) bool {
	return false
}

func (t *Toaster) Update(screen *ebiten.Image) error {
	if !t.started.IsZero() && time.Since(t.started) >= 5*time.Second {
		t.started = time.Time{}

		for _, bread := range t.bread {
			colorM := bread.GetColorM()
			colorM.ChangeHSV(-0.05, 1, 0.75)
			bread.SetColorM(colorM)
		}
	}

	return nil
}

func (t *Toaster) AddBread(bread *Bread) {
	t.bread = append(t.bread, bread)
}
