package main

import (
	"image"
	"log"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/tslocum/venture/pkg/world"
)

type Bread struct {
	*world.Sprite

	id               int
	loaded           bool
	dragging         bool
	toaster          *Toaster
	loadedX, loadedY float64
}

func NewBread(id int, toaster *Toaster) *Bread {
	b := &Bread{id: id, Sprite: world.NewSprite(imgBread, nil), toaster: toaster}

	return b
}

func (b *Bread) MouseDown(x float64, y float64, button ebiten.MouseButton) bool {
	if b.loaded {
		return false
	}

	b.dragging = true

	return false
}

func (b *Bread) Update(screen *ebiten.Image) error {
	if b.dragging && !ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		b.dragging = false

		breadW, breadH := b.GetSize()
		breadX, breadY := b.GetPosition()
		toasterX, toasterY := b.toaster.GetPosition()
		toasterW, toasterH := b.toaster.GetSize()
		if breadX >= toasterX-(breadW/4) && breadX <= toasterX+toasterW+(breadW/4) && breadY >= toasterY-(breadH/4) && breadY <= toasterY+toasterH+(breadH/4) {
			b.loadedX = toasterX + 60 + float64(b.id*25)
			b.loadedY = toasterY - 7 + float64(b.id*7)

			b.toaster.AddBread(b)
			b.loaded = true

			b.SetPosition(b.loadedX, b.loadedY)

			var err error
			b.Image, err = ebiten.NewImageFromImage(b.Image.SubImage(b.Image.Bounds().Sub(image.Point{0, 92})), ebiten.FilterDefault)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	if b.dragging {
		cursorX, cursorY := ebiten.CursorPosition()
		b.SetPosition(float64(cursorX), float64(cursorY))
	}

	return nil
}
