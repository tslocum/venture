package main

import (
	"image/color"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/vector"
	"gitlab.com/tslocum/venture/pkg/world"
)

type PowerPlug struct {
	*world.Sprite

	toaster *Toaster
	outlet  *world.Sprite

	dragging bool
}

func NewPowerPlug(toaster *Toaster, outlet *world.Sprite) *PowerPlug {
	p := &PowerPlug{Sprite: world.NewSprite(imgPowerPlug, nil), toaster: toaster, outlet: outlet}
	return p
}

func (p *PowerPlug) MouseDown(x float64, y float64, button ebiten.MouseButton) bool {
	if p.toaster.powered {
		return false
	}

	p.dragging = true

	return false
}

func (p *PowerPlug) Update(screen *ebiten.Image) error {
	if p.dragging && !ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		p.dragging = false

		plugX, plugY := p.GetPosition()
		outletX, outletY := p.outlet.GetPosition()
		outletW, outletH := p.outlet.GetSize()
		if plugX >= outletX && plugX <= outletX+outletW && plugY >= outletY && plugY <= outletY+outletH {
			p.toaster.powered = true
			p.SetPosition(outletX+24, outletY+17)
		}
	}

	if p.dragging {
		cursorX, cursorY := ebiten.CursorPosition()
		p.SetPosition(float64(cursorX), float64(cursorY))
	}

	return nil
}

func (p *PowerPlug) Draw(screen *ebiten.Image) {
	toasterX, toasterY := p.toaster.GetPosition()
	plugX, plugY := p.GetPosition()

	// TODO Does not display
	var path vector.Path
	path.MoveTo(float32(toasterX), float32(toasterY))
	path.LineTo(float32(plugX), float32(plugY))

	op := &vector.FillOptions{
		Color: color.RGBA{0xdb, 0x56, 0x20, 0xff},
	}
	path.Fill(screen, op)

	p.Sprite.Draw(screen)
}
