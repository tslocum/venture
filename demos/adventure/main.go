package main

import (
	"bytes"
	"image"
	"log"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/tslocum/venture/demos/resources/images"
	"gitlab.com/tslocum/venture/pkg/template/adventure"
	"gitlab.com/tslocum/venture/pkg/world"
)

const screenPadding = 75

var (
	imgToaster       *ebiten.Image
	imgToasterHandle *ebiten.Image
	imgPlate         *ebiten.Image
	imgBread         *ebiten.Image
	imgOutlet        *ebiten.Image
	imgPowerPlug     *ebiten.Image
)

func loadImages() {
	img, _, err := image.Decode(bytes.NewReader(images.Toaster))
	if err != nil {
		log.Fatal(err)
	}
	imgToaster, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	img, _, err = image.Decode(bytes.NewReader(images.ToasterHandle))
	if err != nil {
		log.Fatal(err)
	}
	imgToasterHandle, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	img, _, err = image.Decode(bytes.NewReader(images.Plate))
	if err != nil {
		log.Fatal(err)
	}
	imgPlate, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	img, _, err = image.Decode(bytes.NewReader(images.Bread))
	if err != nil {
		log.Fatal(err)
	}
	imgBread, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	img, _, err = image.Decode(bytes.NewReader(images.Outlet))
	if err != nil {
		log.Fatal(err)
	}
	imgOutlet, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)

	img, _, err = image.Decode(bytes.NewReader(images.PowerPlug))
	if err != nil {
		log.Fatal(err)
	}
	imgPowerPlug, _ = ebiten.NewImageFromImage(img, ebiten.FilterDefault)
}

func main() {
	// Initialize

	ebiten.SetWindowTitle("adventure demo")

	// TODO add controls
	ebiten.SetVsyncEnabled(false)
	//ebiten.SetMaxTPS(120)
	ebiten.SetMaxTPS(ebiten.UncappedTPS)
	//ebiten.SetMaxTPS(60)

	ebiten.SetFullscreen(true)

	loadImages()

	game := adventure.NewAdventureGame()

	toaster := NewToaster()
	game.AddObject(toaster)

	toaster.SetPosition(screenPadding, screenPadding)

	toaster.SetScale(0.5, 0.5)

	toasterW, _ := toaster.GetSize()

	outlet := world.NewSprite(imgOutlet, nil)
	game.Root.AddNode(outlet)

	outlet.SetPosition(screenPadding+toasterW+75, screenPadding+100)

	powerPlug := NewPowerPlug(toaster, outlet)
	game.Root.AddNode(powerPlug)

	powerPlug.SetPosition(screenPadding+toasterW+25, screenPadding+325)

	plate := world.NewSprite(imgPlate, nil)
	game.Root.AddNode(plate)

	plate.SetPosition(screenPadding+toasterW+100, screenPadding+325)

	breadA := NewBread(0, toaster)
	breadA.SetPosition(screenPadding+toasterW+100, screenPadding+300)

	breadB := NewBread(1, toaster)
	breadB.SetPosition(screenPadding+toasterW+145, screenPadding+310)

	game.Root.AddNode(breadA)
	game.Root.AddNode(breadB)

	// Run

	if err := game.Run(); err != nil {
		log.Fatal(err)
	}
}
