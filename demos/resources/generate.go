//go:generate file2byteslice -package=images -input=./images/bread.png -output=./images/bread.go -var=Bread
//go:generate file2byteslice -package=images -input=./images/outlet.png -output=./images/outlet.go -var=Outlet
//go:generate file2byteslice -package=images -input=./images/plate.png -output=./images/plate.go -var=Plate
//go:generate file2byteslice -package=images -input=./images/powerplug.png -output=./images/powerplug.go -var=PowerPlug
//go:generate file2byteslice -package=images -input=./images/toaster.png -output=./images/toaster.go -var=Toaster
//go:generate file2byteslice -package=images -input=./images/toasterhandle.png -output=./images/toasterhandle.go -var=ToasterHandle
package resources
